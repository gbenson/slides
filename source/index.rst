==========
 INFINITY
==========

FOSDEM 2018

Gary Benson <gbenson@redhat.com>

https://infinitynotes.org/f18/

.. Infinity presentation Friday 2016-09-09 1645..1730

.. Abstract: Infinity is a platform-independent system for executables
   and shared libraries to expose functionality to debug, monitoring,
   and analysis tooling.  It grew from a need for GDB to be able to
   debug multithreaded applications without requiring libthread_db.
   Other systems exist that use the libthread_db paradigm, for example
   librtld_db and OPMD; Infinity was designed to replace this entire
   class of library-tool interface with something more portable and
   robust.

.. note::
   * Gary Benson, *gbenson* on IRC
   * Red Hat, Debugger team, GDB

What is Infinity?
=================

A platform-independent system
for executables and shared libraries
to expose functionality
to debug, monitoring, and analysis tooling.

.. note::
   * "Applications"
   * "Software development tools"
   * Infinity is *tool-agnostic*
   * but I may say "debugger" and "GDB" a lot

Motivation
==========

.. note::
   * The threading library
   * Applications using ``libpthread.so`` are doing something *extra*.

Some libraries need to be "understood" by software development tools.

.. rst-class:: build

 * Applications may have multiple threads of execution.

 * This functionality is provided by ``libpthread.so``.

 * Software development tools need to know that applications using
   ``libpthread.so`` can be introspected using functions from
   ``libthread_db.so``.

Motivation (2)
==============

.. figure:: /_static/moti-4boxes.svg
   :class: fill4

.. note::
 * Application is linked to ``libpthread``
 * Tool is accessing the application somehow (red line)
 * Tool has loaded ``libthread_db`` to do what it's doing
 * Straight away there's a fair amount of knowledge we're
   baking into the tool...

Tool Knowledge
==============

.. rst-class:: build

 * All tools need to know that if they find ``libpthread.so``
   loaded then they need ``libthread_db``.

 * Although ``libpthread`` may be statically linked, so the tool might
   need ``libthread_db`` anyway.

 * All tools need to know how to *locate* a *suitable* ``libthread_db``.

 * There's no standard way to do all this, it's just a pattern.

 * ``libpthread`` is just *one* way an application could have
   threads.

.. note::
   * BULLET 1: ``libpthread.so`` filename baked into tool.
   * BULLET 3 (a): Non-static can derive from path.
   * BULLET 3 (b): Version check is just a string, e.g. "2.12"
   * BULLET 4: Inefficient. *Every* tool implements.  For *every* library.
   * **All these issues can be/are handled.**

Motivation (3)
==============

.. figure:: /_static/moti-4boxes.svg
   :class: fill4

.. note::
   This is the graphic we just looked at.

Motivation (4)
==============

.. figure:: /_static/moti-5boxes.svg
   :class: fill5

.. note::
   * All this stuff is linked to the C library
   * Fine if homogenous environment

Motivation (5)
==============

.. figure:: /_static/moti-6boxes.svg
   :class: fill6

.. note::
   * Does not work in heterogenous environment
     * Different architectures (cross-core)
     * Containers (same hardware, different OS, **security**)
   * *Can* make special builds of debug libraries
     * MxN matrix build/organize
     * Poor version checks

What does libthread_db do?
==========================

``libthread_db.so`` exports functions to:

 * Get information about a specific thread
 * Iterate over all threads

``libthread_db.so`` imports functions to:

 * Get the numeric ID of the master process
 * Access memory of process X
 * Access registers of thread Y

.. note::
 * High-level / low-level
 * Tool hands over steering wheel to ``libthread_db.so``
 * ``libthread_db.so`` drives debugger/tool

Solution
========

Platform-independent introspection functions stored in the
libraries they apply to.

.. rst-class:: build

 * Location of suitable functions becomes trivial.

 * Tools don't need to know that library X needs functions Y, Z, A
   that are in library B.

 * Tools can imply that presence of introspection functions implies
   application may be using the functionality they are for.

 * Works nicely with static linking.

.. note::

   So what's the solution?

   * Encode introspection functions in some platform-independent way
   * Put them in the same library the regular code is in
     * Don't need version checks, all built at same time

Solution (2)
============

.. figure:: /_static/infinity-paradigm.svg
   :class: fill4

.. note::
   And here's that as a graphic.

Potential Uses
==============

.. rst-class:: build

 * Thread debugging
 * Multiple linker namespaces (``dlmopen``)
 * OpenMP
 * ASAN and TSAN (`PR glibc/16291
   <https://sourceware.org/bugzilla/show_bug.cgi?id=16291>`_)
 * Pretty printers

.. note::

   * Solaris has ``librtld_db`` for e.g. ``dlmopen``.

   * OpenMP *may or may not have* OMPD, the OpenMP debug library

   * Clang's Address Sanitizer and Thread Sanitizer passes currently
     use hacks to calculate things they need that are not exported.
     Frank asked me if it's the sort of thing Infinity could solve.

   * Pretty printers:
     * Something else from Frank.
     * Could you call a function like ``printf`` from Infinity?
     * Not currently / I've a good idea what's needed / on the roadmap

Components
==========

.. rst-class:: build

 * Note compiler (I8C)
 * Note tester (I8X)
 * Client library (libi8x)
 * GLIBC pthread notes
 * libthread_db integration
 * *GDB integration*

.. note::
   * Flow: note functions developed in parallel with their testcases.
   * Unit tests
   * Late error discovery:
     * ``libpthread`` operates just fine with broken notes.
     * Failure only shows when you try to debug.

libthread_db Function
=====================

.. literalinclude:: td_ta_map_lwp2thr.c

.. note::

   * Comments stripped / not ideal

     * It's getting the address of the symbol ``__stack_user``
     * It's getting the address of the ``next`` field
     * And it's doing something special if ``next`` is NULL.
     * Otherwise it's deferring to ``__td_ta_lookup_th_unique``.

   * ``__stack_user->next == NULL``

     * pthreads still initializing
     * thread register may not have
     * should only be one thread
     * returns ``NULL``, other stuff handles

Infinity Function
=====================

.. literalinclude:: infinity-thread-from_lwpid.i8
   :language: none

.. note::
   * Again comments**+preamble** stripped
   * Compiled to DWARF bytecode
   * Included with metadata

   * Name with namespace
   * One argument pushed
   * Check ``__stack_user->next`` etc
   * I8C *checks* but does not *lay out* stack

Another Infinity Function
=========================

.. literalinclude:: infinity-thread-get_tls_addr.i8
   :language: none

.. note::
   * This is for testcase example
   * More stack manipulation here
   * Makes two calls
   * **One is to runtime linker**

Testcase
========

.. literalinclude:: tst-infinity-thread-get_tls_addr.py

.. note::
   * No flow / one check
   * ``self.implement`` lines stub out other Infinity functions
   * Call with these arguments, returns these values, **checked**
   * Can handle more complex stubs
   * Can build memory

Current Status
==============

.. rst-class:: build

 * Note compiler complete but needs documentation.

 * Note tester complete but needs documentation and more tests.

 * Client library complete but needs documentation and more
   tests (especially validation).

 * GLIBC pthread notes *that GDB needs* are all complete and
   most have tests.  The API the notes create needs documentation.

 * libthread_db integration is complete.  GDB modifications
   to use it are currently native-only.

 * GDB integration is post 1.0.

Demo time!
==========

Further Information
===================

https://infinitynotes.org/

.. rst-class:: build

 * Documentation (wiki)
 * Mailing list
 * Repos
 * Roadmap (Wekan)
