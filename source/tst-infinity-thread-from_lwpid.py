# -*- coding: utf-8 -*-
# Copyright (C) 2015-16 Free Software Foundation, Inc.
#  This file is part of the GNU C Library.
#
#  The GNU C Library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License, or (at your option) any later version.
#
#  The GNU C Library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with the GNU C Library; if not, see
#  <http://www.gnu.org/licenses/>.  */

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from i8c.runtime import TestCase
import struct

TestCase.import_builtin_constants()
TestCase.import_constants_from("infinity-nptl-constants.h")
TestCase.import_constants_from("infinity-nptl_db-constants.h")

class TestThrFromLWPID(TestCase):
    TESTFUNC = "thread::from_lwpid(i)ip"
    MAIN_PID = 30000

    def setUp(self):
        # Set up the test address space for the __stack_user.next
        # dereference.
        with self.memory.builder() as mem:
            stack_user = mem.alloc("__stack_user")
            if self.STACK_USER_SETUP:
                stack_user_next = mem.alloc()
            else:
                stack_user_next = NULL
            stack_user.store_ptr(LIST_T_NEXT_OFFSET, stack_user_next)
        # Initialize flags so we can see what was called.
        self.ps_get_register_count = 0
        self.ps_get_thread_area_count = 0

    def procservice_getpid_impl(self):
        """Implementation of procservice::getpid."""
        return self.MAIN_PID

    def procservice_get_register_impl(self, lwpid, dwreg):
        """Implementation of procservice::get_register."""
        self.assertLess(self.ps_get_register_count, 2)
        result = getattr(self, "PS_GETREG_RESULT", None)
        if result is None:
            self.fail("unexpected ps_get_register")
        self.assertEqual(lwpid, self.lwpid)
        self.assertNotEqual(dwreg, self.lwpid)
        self.assertGreaterEqual(dwreg, 0)
        self.assertLessEqual(dwreg, 66) # Alpha
        self.ps_get_register_count += 1
        return result

    def procservice_get_thread_area_impl(self, lwpid, idx):
        """Implementation of procservice::get_thread_area."""
        self.assertEqual(self.ps_get_thread_area_count, 0)
        result = getattr(self, "PS_GET_TA_RESULT", None)
        if result is None:
            self.fail("unexpected ps_get_thread_area")
        self.assertEqual(lwpid, self.lwpid)
        self.assertNotEqual(idx, self.lwpid)
        self.ps_get_thread_area_count += 1
        return result

    def check_I8_TS_CONST_THREAD_AREA_result(self, result):
        # The result is whatever ps_get_thread_area returned.
        self.assertEqual(self.ps_get_register_count, 0)
        self.assertEqual(self.ps_get_thread_area_count, 1)
        self.assertEqual(result[0], TD_OK)
        self.assertNotEqual(result[1], 0)
        self.assertEqual(result[1], self.PS_GET_TA_RESULT[1])

    def check_I8_TS_REGISTER_result(self, result):
        # The result is what ps_get_register returned with some
        # bias added.  We'll assume the bias is fairly small.
        self.assertGreater(self.ps_get_register_count, 0)
        self.assertLessEqual(self.ps_get_register_count, 2)
        self.assertEqual(self.ps_get_thread_area_count, 0)
        self.assertEqual(result[0], TD_OK)
        self.assertNotEqual(result[1], 0)
        check = self.PS_GETREG_RESULT[1]
        if self.ps_get_register_count > 1:
            # s390x calls this twice
            check = (check << 32) | check
        bias = result[1] - check
        self.assertLess(abs(bias), 0x10000)

    def check_I8_TS_REGISTER_THREAD_AREA_result(self, result):
        # The result is whatever ps_get_thread_area returned.
        self.assertEqual(self.ps_get_register_count, 1)
        self.assertEqual(self.ps_get_thread_area_count, 1)
        self.assertEqual(result[0], TD_OK)
        self.assertNotEqual(result[1], 0)
        self.assertEqual(result[1], self.PS_GET_TA_RESULT[1])

class TestThrFromLWPID_uninit(TestThrFromLWPID):
    STACK_USER_SETUP = False

    def test_uninit(self):
        """Test thread::from_lwpid with NPTL uninitialized"""
        result = self.i8ctx.call(self.TESTFUNC, self.MAIN_PID)
        self.assertEqual(len(result), 2)
        self.assertEqual(result[0], TD_OK)
        self.assertEqual(result[1], NULL)

class TestThrFromLWPID_uninit_wrongpid(TestThrFromLWPID):
    STACK_USER_SETUP = False

    def test_uninit_wrongpid(self):
        """Test from_lwpid with NPTL uninitialized and lwpid != main pid"""
        result = self.i8ctx.call(self.TESTFUNC, self.MAIN_PID + 1)
        self.assertEqual(len(result), 2)
        self.assertEqual(result[0], TD_ERR)

class TestThrFromLWPID_getreg_fail(TestThrFromLWPID):
    STACK_USER_SETUP = True
    PS_GETREG_RESULT = PS_ERR, 0x23ff00fa
    PS_GET_TA_RESULT = PS_OK, 0x89ab1234

    def test_ps_getreg_fail(self):
        """Check thread::from_lwpid handles ps_get_register failures"""
        self.lwpid = self.MAIN_PID + 1
        result = self.i8ctx.call(self.TESTFUNC, self.lwpid)
        self.assertEqual(len(result), 2)
        if self.ps_get_register_count:
            self.assertEqual(result[0], TD_ERR)
        else:
            # This failure isn't a problem for this platform.
            self.check_I8_TS_CONST_THREAD_AREA_result(result)

class TestThrFromLWPID_gta_fail(TestThrFromLWPID):
    STACK_USER_SETUP = True
    PS_GETREG_RESULT = PS_OK, 0x23ff00fa
    PS_GET_TA_RESULT = PS_ERR, 0x89ab1234

    def test_ps_gta_fail(self):
        """Check thread::from_lwpid handles ps_get_thread_area failures"""
        self.lwpid = self.MAIN_PID + 1
        result = self.i8ctx.call(self.TESTFUNC, self.lwpid)
        self.assertEqual(len(result), 2)
        if self.ps_get_thread_area_count:
            self.assertEqual(result[0], TD_ERR)
        else:
            # This failure isn't a problem for this platform.
            self.check_I8_TS_REGISTER_result(result)

class TestThrFromLWPID_mainpath(TestThrFromLWPID):
    STACK_USER_SETUP = True
    PS_GETREG_RESULT = PS_OK, 0x23ff00fa
    PS_GET_TA_RESULT = PS_OK, 0x89ab1234

    def test_mainpath(self):
        """Test the main path through thread::from_lwpid"""
        self.lwpid = self.MAIN_PID + 1
        result = self.i8ctx.call(self.TESTFUNC, self.lwpid)
        self.assertEqual(len(result), 2)
        if self.ps_get_thread_area_count:
            if self.ps_get_register_count:
                self.check_I8_TS_REGISTER_THREAD_AREA_result(result)
            else:
                self.check_I8_TS_CONST_THREAD_AREA_result(result)
        else:
            self.check_I8_TS_REGISTER_result(result)
