class TestThrGetTLSAddr(TestCase):
    TESTFUNC = "thread::get_tls_addr(ppi)ip"

    def do_test(self, descr, lm, offset, modid, code, base):
        # Stub out the functions thread::get_tls_addr calls.
        self.implement("rtld::__lm_tls_modid", (lm,), (modid,))
        self.implement("thread::get_tlsbase", (descr, modid), (code, base))
        # Run the test.
        result = self.i8ctx.call(self.TESTFUNC, descr, lm, offset)
        self.assertEqual(len(result), 2)
        self.assertEqual(result[0], code)
        # This next is strictly only necessary if code == TD_OK,
        # but our implementation always calculates it.
        self.assertEqual(result[1], base + offset)

    def test_get_tls_addr(self):
        """Check thread::get_tls_addr works"""
        self.do_test(1019, 2371, 3259, 4507, 5981, 6427)
